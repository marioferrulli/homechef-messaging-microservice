package com.homechef.messagingservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "MESSAGES")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idMessage;

    private int idSender;
    private int idReceiver;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date msg_timestamp;

    private String msg_text;

    @PrePersist
    public void addTimestamp() {
        msg_timestamp = new Date();
    }

    public Message(int idSender, int idReceiver, Date msg_timestamp, String msg_text) {
        this.idSender = idSender;
        this.idReceiver = idReceiver;
        this.msg_timestamp = msg_timestamp;
        this.msg_text = msg_text;
    }

    public Message() {
    }

    public int getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(int idMessage) {
        this.idMessage = idMessage;
    }

    public int getIdSender() {
        return idSender;
    }

    public void setIdSender(int idSender) {
        this.idSender = idSender;
    }

    public int getIdReceiver() {
        return idReceiver;
    }

    public void setIdReceiver(int idReceiver) {
        this.idReceiver = idReceiver;
    }

    public Date getMsg_timestamp() {
        return msg_timestamp;
    }

    public void setMsg_timestamp(Date msg_timestamp) {
        this.msg_timestamp = msg_timestamp;
    }

    public String getMsg_text() {
        return msg_text;
    }

    public void setMsg_text(String msg_text) {
        this.msg_text = msg_text;
    }
}
