package com.homechef.messagingservice.dao;

import com.homechef.messagingservice.model.Conversations;
import com.homechef.messagingservice.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Integer> {

    List<Message> findAllByIdSenderAndIdReceiver(Integer sender, Integer receiver);

    @Query(value =
            "select id_message, " +
                    "id_receiver, " +
                    "id_sender, " +
                    "msg_text, " +
                    "msg_timestamp " +
                    "from messages " +
                    "where (id_receiver = :user1 and id_sender = :user2) " +
                    "OR (id_receiver = :user2 and id_sender = :user1)" +
                    " order by msg_timestamp asc", nativeQuery = true)
    List<Message> getOrderedConversation(Integer user1, Integer user2);


    @Query(value = "select id_message, id_receiver, id_sender, msg_text, msg_timestamp from messages where id_sender = :userInvolved or id_receiver = :userInvolved group by id_receiver, id_sender", nativeQuery = true)
    List<Message> getAllConversations(Integer userInvolved);


}
