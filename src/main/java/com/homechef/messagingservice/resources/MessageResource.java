package com.homechef.messagingservice.resources;

import com.homechef.messagingservice.dao.MessageRepository;
import com.homechef.messagingservice.model.Chat;
import com.homechef.messagingservice.model.Conversations;
import com.homechef.messagingservice.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/messages")
public class MessageResource {

    @Autowired
    MessageRepository messageRepository;

    /*
    @GetMapping(path="/getByDateOrderA")
    public Iterable<Insertion> findAllByOrderByDateAsc() {
        return insertionRepository.findAllByOrderByDateAsc();
    }
     */

    @PostMapping(path = "/addMessage")
    public String addMessage(@RequestBody Message message){
        messageRepository.save(message);
        return "saved";
    }

    /*
    @GetMapping(path = "/getConversation/")
    public Chat getConversation(@RequestParam("sender") int sender, @RequestParam("receiver") int receiver){
        List<Message> messageList = messageRepository.findAllByIdSenderAndIdReceiver(sender, receiver);
        Chat chat = new Chat();
        chat.setMessageList(messageList);
        return chat;
    }
     */

    @GetMapping(path="/getOrderedConversation")
    public Chat findAllMessagesByOrderByDateAsc(@RequestParam("user1") int user1, @RequestParam("user2") int user2) {
        List<Message> orderedConversation = messageRepository.getOrderedConversation(user1, user2);
        Chat chat = new Chat();
        chat.setMessageList(orderedConversation);
        return chat;
    }

    @PostMapping(path = "/addMessageAndGetConversation")
    public Chat addMessageAndReturnConversation(@RequestBody Message message){
        messageRepository.saveAndFlush(message);
        int user1 = message.getIdSender();
        int user2 = message.getIdReceiver();
        List<Message> messageList = messageRepository.getOrderedConversation(user1, user2);
        Chat chat = new Chat();
        chat.setMessageList(messageList);
        return chat;
    }

    @GetMapping(path = "/getAllConversations/{idUser}")
    public Conversations getAllConversation(@PathVariable("idUser") int idUser){
        List<Message> messageList = messageRepository.getAllConversations(idUser);
        Conversations conversations = new Conversations();
        conversations.setConversations(messageList);
        return conversations;

    }




}
