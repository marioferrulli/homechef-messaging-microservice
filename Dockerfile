FROM openjdk:8
ADD target/messaging-service.jar messaging-service.jar
EXPOSE 8081
COPY wait-for-it.sh wait-for-it.sh
RUN chmod +x wait-for-it.sh